## Nefunkciniai reikalavimai
1. Naudojamas Laravel 5.6 karkasas.
2. Nuotraukoms turi būti naudojamas polimorfinis sąryšis.

## Funkciniai reikalavimai
1. Sistema palaiko tris naudotojų rūšis: administratorius, vartotojas ir svečias.
2. Sistema geba parodyti visus aktoriaus filmus.
3. Sistema leidžia administratoriui valdyti filmus (CRUD).
4. Sistema leidžia administratoriui valdyti aktorius (CRUD).
5. Sistema leidžia vartotojams pridedinėti filmų ir aktorių nuotraukas.
6. Sistema leidžia administratoriui valdyti vartotojus ir skirti jiems roles.
7. Sistema leidžia užsiregistruoti vartotojams.
8. Sistema leidžia svečiams tik matyti turinį.
9. Sistema seka, kuris vartotojas sukūrė tam tikrą turinį.
10. Sistema kategorizuoja filmus.
11. Sistema leidžia administratoriui valdyti kategorijas (CRUD).

## Dalykinės srities modelis
![Dalykinės srities modelis](images/domain.png)

## Duomenų bazės struktūra
![Duomenų bazės struktūra](images/db.png)
